// ***** global constants

// directions
global DI_UP       =0;
global DI_UPRIGHT  =1;
global DI_RIGHT    =2;
global DI_DOWNRIGHT=3;
global DI_DOWN     =4;
global DI_DOWNLEFT =5;
global DI_LEFT     =6;
global DI_UPLEFT   =7;


// text alignment
global TAL_LEFT  =0;
global TAL_RIGHT =1;
global TAL_CENTER=2;


// vertical text alignment
global VAL_TOP=0;
global VAL_CENTER=1;
global VAL_BOTTOM=2;

global LONG_CLICK_TIME_MS             = 700;
global MIN_CAPTION_DISPLAY_TIME_MS    = 400;
global CURSOR_DISPLAY_TIME_MS         = 5000;
global CURSOR_DISPLAY_MOVE_EVENT_TIME = 100;
