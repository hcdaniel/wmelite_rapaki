#ifndef __StoreKit_functions_H__
#define __StoreKit_functions_H__

#include "textencoding.h"

#ifdef __cplusplus
extern "C" {
#endif

    void StoreKit_SetExternalData(void* data);
    
    int StoreKit_IsStoreAvailable();

    void StoreKit_EnableEvents();
    
    void StoreKit_DisableEvents();
    
    void StoreKit_ValidateProducts(const char* ids, TTextEncoding encoding);
    
    void StoreKit_Purchase(const char* prodId, TTextEncoding encoding);
    
    void StoreKit_RestoreTransactions(TTextEncoding encoding);
    
    int StoreKit_FinishTransaction(const char* transId, TTextEncoding encoding);
    
#ifdef __cplusplus
}
#endif


#endif // __StoreKit_functions_H__
