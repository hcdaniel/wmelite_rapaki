#ifndef DISPLAY_CONFIG_H
#define DISPLAY_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

	void SetKeepDisplayOn(int keepOn);

#ifdef __cplusplus
}
#endif


#endif // DISPLAY_CONFIG_H
