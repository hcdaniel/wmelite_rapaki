#ifndef WINRT_VERSION_INFO_H
#define WINRT_VERSION_INFO_H

extern "C" void WinRTVersionInfo(char *info, int maxlen);
extern "C" void WinRTAppInfo(char *info, int maxlen);

#endif //WINRT_VERSION_INFO_H
